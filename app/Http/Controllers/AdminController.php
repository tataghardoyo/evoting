<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
        return view('homeadmin');
    }
    public function create()
    {
        return view('tambahsiswa');
    }
    public function edit()
    {
        return view('editsiswa');
    }
}
