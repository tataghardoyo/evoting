<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Berita;

class BerandaController extends Controller
{
    public function index()
    {
        $berita = Berita::all();
        return view('welcome',compact('berita'));
    }
    public function show($berita_id)
    {
        $berita = Berita::find($berita_id);
        return view('lamanberita', compact('berita'));
    }
}