<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CalonManajemenController extends Controller
{
    public function index()
    {
        return view('calonmanajemen');
    }
    public function create()
    {
        return view('tambahcalon');
    }
    public function edit()
    {
        return view('editcalon');
    }
}