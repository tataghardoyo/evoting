<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Berita;

class PengumumanManajemenController extends Controller
{
    public function index()
    {
        $berita = Berita::all();
        return view('pengumumanmanajemen', compact('berita'));
    }
    public function create()
    {
        return view('tambahpengumuman');
    }
    public function edit($berita_id)
    {
        $berita = Berita::find($berita_id);
        return view('editpengumuman', compact('berita'));
    }
    public function store(Request $request)
    {
        $this->validate($request,[
    		'judulberita' => 'required|max:255',
    		'deskripsi' => 'required',
            'gambar' => 'required|mimes:jpeg,jpg,png'
    	]);

        $gambarberita = $request->gambar;
        $new_gambar = time(). ' - '.$gambarberita->getClientOriginalName();
        $gambarberita->move('img/', $new_gambar);

        Berita::create([
    		'judulberita' => $request->judulberita,
    		'deskripsi' => $request->deskripsi,
            'gambar' => $new_gambar
    	]);
 
    	return redirect('/pengumumanmanajemen');
    }

    public function update($berita_id, Request $request)
    {
        $this->validate($request,[
    		'judulberita' => 'required|max:255',
    		'deskripsi' => 'required',
            'gambar' => 'mimes:jpeg,jpg,png'
    	]);
       
        
        $berita = Berita::findorfail($berita_id);
        if($request->has('gambar')){
            $path = public_path()."/img/".$berita->gambar;
            unlink($path);
            $gambar = $request->gambar;
            
            $new_gambar = time(). ' - '.$gambar->getClientOriginalName();
            
            $gambar->move('img/', $new_gambar);

            $databerita = [
                'judulberita' => $request->judulberita,
                'deskripsi' => $request->deskripsi,
                'gambar' => $new_gambar,
            ];
            
        }else{
            $databerita = [
                'judulberita' => $request->judulberita,
                'deskripsi' => $request->deskripsi,
            ];
          
        }
       
        $berita->update($databerita);

        return redirect('/pengumumanmanajemen');
    }

    public function destroy($berita_id)
    {
        $berita = Berita::find($berita_id);
        $berita->delete();
        return redirect('/pengumumanmanajemen');
    }
}