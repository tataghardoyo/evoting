@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 mt-4">
            <div class="card p-4 shadow">
                <h3 class="text-center">Ubah Password</h3>
                <form>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password Lama</label>
                        <input type="password" class="form-control" id="passwordlama" name="passwordlama"
                            placeholder="Masukkan Password Lama">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password Baru</label>
                        <input type="password" class="form-control" id="paswordbaru" name="paswordbaru"
                            placeholder="Masukkan Password Baru">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Ulangi Password</label>
                        <input type="password" class="form-control" id="ulangipassword" name="ulangipassword"
                            placeholder="Ulangi Password Baru">
                    </div>
                    <div class="d-flex justify-content-center">
                        <button type="submit" class="btn btn-primary">Ubah</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection