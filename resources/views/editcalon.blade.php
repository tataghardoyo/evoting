@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row d-flex flex-column bg-white border rounded">
        <div class="mx-auto">
            <h1 class="text-center mt-2">Manajemen Data Vote</h1>
            <p class="text-center">website e-voting Ketua Osis Smada</p>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-sm-4">
            <div class="bg-white border rounded p-3 mb-3">
                <ul class="list-group">
                    <a class="nav-link font-weight-bold {{ (request()->is('dashboard')) ? 'text-dark shadow' : '' }}"
                        href="{{url('/dashboard')}}">
                        <li class="list-group-item">Data Siswa</li>
                    </a>
                    <a class="nav-link font-weight-bold {{ (request()->is('calonmanajemen')) ? 'text-dark shadow' : '' }}"
                        href="{{url('/calonmanajemen')}}">
                        <li class="list-group-item">Data Calon</li>
                    </a>
                    <a class="nav-link font-weight-bold {{ (request()->is('votemanajemen')) ? 'text-dark shadow' : '' }}"
                        href="{{url('/votemanajemen')}}">
                        <li class="list-group-item">Data Vote</li>
                    </a>

                    <a class="nav-link font-weight-bold {{ (request()->is('pengumumanmanajemen')) ? 'text-dark shadow' : '' }}"
                        href="{{url('/pengumumanmanajemen')}}">
                        <li class="list-group-item">Data Pengumuman</li>
                    </a>
                </ul>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="bg-white border rounded p-3 mb-3">
                Halaman Data Calon / Edit Calon
            </div>
            <div class="bg-white border rounded p-3 mb-3">
                <form>
                    <div class="form-group">
                        <label for="namacalon">Nama Calon</label>
                        <input type="text" class="form-control" id="namacalon" name="namacalon"
                            placeholder="Masukkan Nama Calon">
                    </div>
                    <div class="form-group">
                        <label for="nourut">No Urut</label>
                        <input type="number" class="form-control" id="nourut" name="nourut"
                            placeholder="Masukkan No Urut">
                    </div>
                    <div class="form-group">
                        <label for="visi">Visi</label>
                        <input type="text" class="form-control" id="visi" name="visi" placeholder="Masukkan Visi">
                    </div>
                    <div class="form-group">
                        <label for="misi">Misi</label>
                        <textarea class="form-control"  name="misi" id="misi" cols="30" rows="10"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="namacalon">Foto</label>
                        <input type="file" accept=".png, .jpg, .jpeg" class="form-control" id="foto" name="foto">
                    </div>
                    <button type="submit" class="btn btn-primary">Edit</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection