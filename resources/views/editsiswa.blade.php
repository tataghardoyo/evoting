@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row d-flex flex-column bg-white border rounded">
        <div class="mx-auto">
            <h1 class="text-center mt-2">Manajemen Data Vote</h1>
            <p class="text-center">website e-voting Ketua Osis Smada</p>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-sm-4">
            <div class="bg-white border rounded p-3 mb-3">
                <ul class="list-group">
                    <a class="nav-link font-weight-bold {{ (request()->is('dashboard')) ? 'text-dark shadow' : '' }}"
                        href="{{url('/dashboard')}}">
                        <li class="list-group-item">Data Siswa</li>
                    </a>
                    <a class="nav-link font-weight-bold {{ (request()->is('calonmanajemen')) ? 'text-dark shadow' : '' }}"
                        href="{{url('/calonmanajemen')}}">
                        <li class="list-group-item">Data Calon</li>
                    </a>
                    <a class="nav-link font-weight-bold {{ (request()->is('votemanajemen')) ? 'text-dark shadow' : '' }}"
                        href="{{url('/votemanajemen')}}">
                        <li class="list-group-item">Data Vote</li>
                    </a>

                    <a class="nav-link font-weight-bold {{ (request()->is('pengumumanmanajemen')) ? 'text-dark shadow' : '' }}"
                        href="{{url('/pengumumanmanajemen')}}">
                        <li class="list-group-item">Data Pengumuman</li>
                    </a>
                </ul>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="bg-white border rounded p-3 mb-3">
                Halaman Data Siswa / Edit Siswa
            </div>
            <div class="bg-white border rounded p-3 mb-3">
                <form>
                    <div class="form-group">
                        <label for="namacalon">Nama Siswa</label>
                        <input type="text" class="form-control" id="namasiswa" name="namasiswa"
                            placeholder="Masukkan Nama Siswa">
                    </div>
                    <div class="form-group">
                        <label for="nourut">Email</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Masukkan Email">
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" name="password"
                            placeholder="Masukkan Password">
                    </div>
                    <div class="form-group">
                        <label for="kelas">Kelas</label>
                        <select class="form-control" name="kelas" id="kelas">
                            <option value="" disabled>-- Pilih Kelas --</option>
                            <option value="10A1">10A1</option>
                            <option value="10A1">10A2</option>
                            <option value="10A1">10S1</option>
                            <option value="10A1">11A1</option>
                            <option value="10A1">11A2</option>
                            <option value="10A1">11S1</option>
                            <option value="10A1">12A1</option>
                            <option value="10A1">12A2</option>
                            <option value="10A1">12S1</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="statusvote">Status Vote</label>
                        <input type="text" class="form-control" id="statusvote" name="statusvote" value="Belum Vote" disabled="disabled">
                    </div>
                    <button type="submit" class="btn btn-primary">Edit</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection