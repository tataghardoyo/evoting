@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row d-flex flex-column bg-white border rounded">
        <div class="mx-auto">
            <h1 class="text-center mt-2">Pemilihan Calon Ketua Osis</h1>
            <p class="text-center">website e-voting Ketua Osis Smada</p>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-sm mb-3">
            <div class="card mx-auto shadow" style="width: 18rem;">
                <img class="card-img-top" src="https://placeimg.com/300/200/arch" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title text-center font-weight-bold">Tono-Tini</h5>
                    <p class="card-text text-center display-4">01</p>
                    <div class="d-flex justify-content-center">
                        <a href="#" class="btn btn-primary">VOTE</a>
                    </div>
                    <div class="d-flex justify-content-end">
                        <a class="text-dark" href="{{url('/visimisi')}}"><i class="fa fa-ellipsis-v fa-1x"
                                aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm mb-3">
            <div class="card mx-auto shadow" style="width: 18rem;">
                <img class="card-img-top" src="https://placeimg.com/300/200/arch" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title text-center font-weight-bold">Tono-Tini</h5>
                    <p class="card-text text-center display-4">02</p>
                    <div class="d-flex justify-content-center">
                        <a href="#" class="btn btn-primary {{$status !=='buka' || $vote ==='sudah' ? 'disabled':''}}">VOTE</a>
                    </div>
                    <div class="d-flex justify-content-end">
                        <a class="text-dark" href="{{url('/visimisi')}}"><i class="fa fa-ellipsis-v fa-1x"
                                aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm mb-3">
            <div class="card mx-auto shadow" style="width: 18rem;">
                <img class="card-img-top" src="https://placeimg.com/300/200/arch" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title text-center font-weight-bold">Tono-Tini</h5>
                    <p class="card-text text-center display-4">03</p>
                    
                    <div class="d-flex justify-content-center">
                        <a href="#" class="btn btn-primary">VOTE</a>
                    </div>
                    <div class="d-flex justify-content-end">
                        <a class="text-dark" href="{{url('/visimisi')}}"><i class="fa fa-ellipsis-v fa-1x"
                                aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection