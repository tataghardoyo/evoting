@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row d-flex flex-column bg-white border rounded">
        <div class="mx-auto">
            <h1 class="text-center mt-2">Manajemen Data Vote</h1>
            <p class="text-center">website e-voting Ketua Osis Smada</p>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-sm-4">
            <div class="bg-white border rounded p-3 mb-3">
                <ul class="list-group">
                    <a class="nav-link font-weight-bold {{ (request()->is('dashboard')) ? 'text-dark shadow' : '' }}"
                        href="{{url('/dashboard')}}">
                        <li class="list-group-item">Data Siswa</li>
                    </a>
                    <a class="nav-link font-weight-bold {{ (request()->is('calonmanajemen')) ? 'text-dark shadow' : '' }}"
                        href="{{url('/calonmanajemen')}}">
                        <li class="list-group-item">Data Calon</li>
                    </a>
                    <a class="nav-link font-weight-bold {{ (request()->is('votemanajemen')) ? 'text-dark shadow' : '' }}"
                        href="{{url('/votemanajemen')}}">
                        <li class="list-group-item">Data Vote</li>
                    </a>

                    <a class="nav-link font-weight-bold {{ (request()->is('pengumumanmanajemen')) ? 'text-dark shadow' : '' }}"
                        href="{{url('/pengumumanmanajemen')}}">
                        <li class="list-group-item">Data Pengumuman</li>
                    </a>
                </ul>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="bg-white border rounded p-3 mb-3">
                Halaman Data Siswa
            </div>
            <div class="bg-white border rounded p-3 mb-3">
                <a href="{{url('/createsiswa')}}" class="btn btn-success mb-3 font-weight-bold">+ Siswa</a>
                <div class="bg-white border rounded px-auto mb-3 overflow-auto">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nama</th>
                                <th scope="col">Email</th>
                                <th scope="col">Kelas</th>
                                <th scope="col">Status</th>
                                <th scope="col">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">1</th>
                                <td>Mark Antony wens</td>
                                <td>otto@gmail.co.id</td>
                                <td>10A3</td>
                                <td>sudah</td>
                                <td>
                                    <div style="width:60px">
                                        <a href="{{url('/editsiswa')}}" class="btn-sm btn-primary"><i class="fas fa-edit"></i></a>
                                        <a href="" class="btn-sm btn-danger"><i class="fas fa-trash-alt"></i></a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">2</th>
                                <td>Jacob Henderson</td>
                                <td>jacob@mail.com</td>
                                <td>11S2</td>
                                <td>belum</td>
                                <td>
                                    <div style="width:60px">
                                        <a href="" class="btn-sm btn-primary"><i class="fas fa-edit"></i></a>
                                        <a href="" class="btn-sm btn-danger"><i class="fas fa-trash-alt"></i></a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">3</th>
                                <td>Larry Supardi</td>
                                <td>larry@mail.com</td>
                                <td>12A1</td>
                                <td>sudah</td>
                                <td>
                                    <div style="width:60px">
                                        <a href="" class="btn-sm btn-primary"><i class="fas fa-edit"></i></a>
                                        <a href="" class="btn-sm btn-danger"><i class="fas fa-trash-alt"></i></a>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection