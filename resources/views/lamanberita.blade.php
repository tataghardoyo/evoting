@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row d-flex flex-column bg-white border rounded">
        <div class="mx-auto">
            <h1 class="text-center mt-2">Laman Berita</h1>
            <p class="text-center">Calon Ketua Osis Smada</p>
        </div>
    </div>
    <div class="row d-flex flex-column bg-white border rounded mt-2">
        <div class="p-4">
            <h2 class="text-start mt-2 mb-4">{{$berita->judulberita}}</h2>
            <img src="/img/{{$berita->gambar}}" alt="gambar" class="w-100 h-auto rounded">
            <p class="mt-3 text-justify">
               {{$berita->deskripsi}}
            </p>
            <p>
           <em>Diupload tanggal : {{$berita->updated_at}}</em>
            </p>
        </div>
    </div>
</div>
@endsection