@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row d-flex flex-column bg-white border rounded">
        <div class="mx-auto">
            <h1 class="text-center mt-2">Manajemen Data Vote</h1>
            <p class="text-center">website e-voting Ketua Osis Smada</p>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-sm-4">
            <div class="bg-white border rounded p-3 mb-3">
                <ul class="list-group">
                    <a class="nav-link font-weight-bold {{ (request()->is('dashboard')) ? 'text-dark shadow' : '' }}"
                        href="{{url('/dashboard')}}">
                        <li class="list-group-item">Data Siswa</li>
                    </a>
                    <a class="nav-link font-weight-bold {{ (request()->is('calonmanajemen')) ? 'text-dark shadow' : '' }}"
                        href="{{url('/calonmanajemen')}}">
                        <li class="list-group-item">Data Calon</li>
                    </a>
                    <a class="nav-link font-weight-bold {{ (request()->is('votemanajemen')) ? 'text-dark shadow' : '' }}"
                        href="{{url('/votemanajemen')}}">
                        <li class="list-group-item">Data Vote</li>
                    </a>

                    <a class="nav-link font-weight-bold {{ (request()->is('pengumumanmanajemen')) ? 'text-dark shadow' : '' }}"
                        href="{{url('/pengumumanmanajemen')}}">
                        <li class="list-group-item">Data Pengumuman</li>
                    </a>
                </ul>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="bg-white border rounded p-3 mb-3">
                Halaman Data Pengumuman
            </div>
            <div class="bg-white border rounded p-3">
                <a href="{{url('/createpengumuman')}}" class="btn btn-success mb-3 font-weight-bold">+ Pengumuman</a>
                <div class="bg-white rounded px-auto overflow-auto">
                    <table class="table table-bordered">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Judul</th>
                                <th scope="col">Deskripsi</th>
                                <th scope="col">Gambar</th>
                                <th scope="col">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($berita as $key=>$value)
                            <tr>
                                <th scope="row">{{$key + 1 }}</th>
                                <td>
                                    {{$value->judulberita}}
                                </td>
                                <td>
                                    <p class="text-justify" style="word-break: break-all;">
                                        {{Str::limit($value->deskripsi, 120)}}
                                    </p>
                                </td>
                                <td>
                                    <img class="rounded" src="./img/{{$value->gambar}}" alt="gambar" width="150"
                                        height="100">
                                </td>
                                <td>
                                    <div style="width:60px">
                                        <a href="/editpengumuman/{{$value->id}}" class="btn-sm btn-primary"><i
                                                class="fas fa-edit"></i></a>
                                        <a href="{{ route('task.destroy',$value->id) }}" onclick="return myFunction();"
                                            class="btn-sm btn-danger"><i class="fas fa-trash-alt"></i></a>
                                    </div>
                                </td>
                            </tr>
                            @empty
                            <tr colspan="3">
                                <td>No data</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@push('script')
<script>
function myFunction() {
    if (!confirm("Are You Sure to delete this"))
        event.preventDefault();
}
</script>
@endpush

@endsection