@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row d-flex flex-column bg-white border rounded">
        <div class="mx-auto">
            <h1 class="text-center mt-2">Manajemen Data Vote</h1>
            <p class="text-center">website e-voting Ketua Osis Smada</p>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-sm-4">
            <div class="bg-white border rounded p-3 mb-3">
                <ul class="list-group">
                    <a class="nav-link font-weight-bold {{ (request()->is('dashboard')) ? 'text-dark shadow' : '' }}"
                        href="{{url('/dashboard')}}">
                        <li class="list-group-item">Data Siswa</li>
                    </a>
                    <a class="nav-link font-weight-bold {{ (request()->is('calonmanajemen')) ? 'text-dark shadow' : '' }}"
                        href="{{url('/calonmanajemen')}}">
                        <li class="list-group-item">Data Calon</li>
                    </a>
                    <a class="nav-link font-weight-bold {{ (request()->is('votemanajemen')) ? 'text-dark shadow' : '' }}"
                        href="{{url('/votemanajemen')}}">
                        <li class="list-group-item">Data Vote</li>
                    </a>

                    <a class="nav-link font-weight-bold {{ (request()->is('pengumumanmanajemen')) ? 'text-dark shadow' : '' }}"
                        href="{{url('/pengumumanmanajemen')}}">
                        <li class="list-group-item">Data Pengumuman</li>
                    </a>
                </ul>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="bg-white border rounded p-3 mb-3">
                Halaman Data Pengumuman / Tambah Berita
            </div>
            <div class="bg-white border rounded p-3 mb-3">
                <form method="POST" action="{{ url('createpengumuman') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="judulberita">Judul Berita</label>
                        <input type="text" class="form-control" id="judulberita" name="judulberita"
                            placeholder="Masukkan Judul Berita">
                        @error('judulberita')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="deskripsi">Deskripsi</label>
                        <textarea class="form-control" name="deskripsi" id="deskripsi" cols="30" rows="10"></textarea>
                        @error('deskripsi')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="gambar">Gambar</label>
                        <input type="file" accept=".png, .jpg, .jpeg" class="form-control" id="gambar" name="gambar">
                        @error('gambar')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-success">Tambah</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection