@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row d-flex flex-column bg-white border rounded">
        <div class="mx-auto">
            <h1 class="text-center mt-2">VISI & MISI</h1>
            <p class="text-center">Calon Ketua Osis Smada</p>
        </div>
    </div>
    <div class="row d-flex flex-column bg-white border rounded mt-2">
        <div class="p-4 text-center">
 
           <div style="width:400px; margin:10px auto;">
           <img src="https://placeimg.com/600/400/arch" alt="gambar" class="w-100 rounded">
           </div>
        
            <h2 class="mt-3 mb-1">Tono-Tini</h2>
            <p class="display-1">01</p>
            <h4>Visi</h4>
            <p class="mt-3 text-justify">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. At sit magnam nihil nulla consequuntur ut
                aliquam pariatur ipsa eius, voluptas totam tempore tenetur dicta voluptates. Culpa sapiente est
                obcaecati esse!. Lorem ipsum dolor sit amet consectetur adipisicing elit. At sit magnam nihil nulla consequuntur ut
                aliquam pariatur ipsa eius, voluptas totam tempore tenetur dicta voluptates. Culpa sapiente est
                obcaecati esse!. Lorem ipsum dolor sit amet consectetur adipisicing elit. At sit magnam nihil nulla consequuntur ut
                aliquam pariatur ipsa eius, voluptas totam tempore tenetur dicta voluptates. Culpa sapiente est
                obcaecati esse!
            </p>
            <h4>Misi</h4>
            <p class="mt-3 text-justify">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. At sit magnam nihil nulla consequuntur ut
                aliquam pariatur ipsa eius, voluptas totam tempore tenetur dicta voluptates. Culpa sapiente est
                obcaecati esse!. Lorem ipsum dolor sit amet consectetur adipisicing elit. At sit magnam nihil nulla consequuntur ut
                aliquam pariatur ipsa eius, voluptas totam tempore tenetur dicta voluptates. Culpa sapiente est
                obcaecati esse!. Lorem ipsum dolor sit amet consectetur adipisicing elit. At sit magnam nihil nulla consequuntur ut
                aliquam pariatur ipsa eius, voluptas totam tempore tenetur dicta voluptates. Culpa sapiente est
                obcaecati esse!
            </p>
        </div>
    </div>
</div>
@endsection