@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row d-flex flex-column bg-white border rounded">
        <div class="mx-auto">
            <h1 class="text-center mt-2">Manajemen Data Vote</h1>
            <p class="text-center">website e-voting Ketua Osis Smada</p>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-sm-4">
            <div class="bg-white border rounded p-3 mb-3">
                <ul class="list-group">
                    <a class="nav-link font-weight-bold {{ (request()->is('dashboard')) ? 'text-dark shadow' : '' }}"
                        href="{{url('/dashboard')}}">
                        <li class="list-group-item">Data Siswa</li>
                    </a>
                    <a class="nav-link font-weight-bold {{ (request()->is('calonmanajemen')) ? 'text-dark shadow' : '' }}"
                        href="{{url('/calonmanajemen')}}">
                        <li class="list-group-item">Data Calon</li>
                    </a>
                    <a class="nav-link font-weight-bold {{ (request()->is('votemanajemen')) ? 'text-dark shadow' : '' }}"
                        href="{{url('/votemanajemen')}}">
                        <li class="list-group-item">Data Vote</li>
                    </a>

                    <a class="nav-link font-weight-bold {{ (request()->is('pengumumanmanajemen')) ? 'text-dark shadow' : '' }}"
                        href="{{url('/pengumumanmanajemen')}}">
                        <li class="list-group-item">Data Pengumuman</li>
                    </a>
                </ul>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="bg-white border rounded p-3 mb-3">
                Halaman Data Vote
            </div>
            <div class="bg-white border rounded p-3 mb-3">
                Tanggal Vote
                <input class="ml-3" type="text" value="23-10-21" disabled="disabled">
                <a href="{{url('/settingvote')}}" class="btn btn-warning">Setting Vote</a>
            </div>
            <div class="bg-white border rounded p-3 mb-3">
                Hasil Vote
                <canvas id="myChart" style="width:100%;max-width:600px"></canvas>
            </div>
            <div class="bg-white border rounded p-3 mb-3">
                <button class="btn btn-danger mb-3 font-weight-bold">Reset Data Vote</button>
                <div class="bg-white border rounded px-auto mb-3 overflow-auto">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Siswa</th>
                                <th scope="col">Calon</th>
                                <th scope="col">Tanggal Vote</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">1</th>
                                <td>Mark Antony wens</td>
                                <td>Nono-Nini</td>
                                <td>17-08-2021 12:00:00</td>
                            </tr>
                            <tr>
                                <th scope="row">2</th>
                                <td>Jacob Henderson</td>
                                <td>Tono-Tini</td>
                                <td>17-08-2021 12:00:00</td>
                            </tr>
                            <tr>
                                <th scope="row">3</th>
                                <td>Lucas Mandowen</td>
                                <td>Bowo-Joko</td>
                                <td>17-08-2021 12:00:00</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="4">
                                    Total Siswa Voting : 1200
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@push('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"></script>
<script>
var xValues = ["Tono-Tini", "Nono-Nini", "Bowo-Joko"];
var yValues = [45, 15, 40];
var barColors = [
  "#b91d47",
  "#00aba9",
  "#2b5797",
  "#ffff00",
  "#00ff00",
  "#f5d7b5",
  "#ff6666",
  "#b698f9",
  "#ff7f50",
  "#ffc3a0",
];

new Chart("myChart", {
  type: "pie",
  data: {
    labels: xValues,
    datasets: [{
      backgroundColor: barColors,
      data: yValues
    }]
  },
  options: {
    title: {
      display: true,
      text: "Rekapitulasi Hasil Vote calon Ketua osis 2021"
    }
  }
});
</script>
@endpush
@endsection
