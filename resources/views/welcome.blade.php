@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row d-flex flex-column bg-white border rounded">
        <div class="mx-auto">
            <h1 class="text-center mt-2">Selamat datang</h1>
            <p class="text-center">website e-voting Ketua Osis Smada</p>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-sm-4">
            <div class="bg-white border rounded p-3 mb-3">
                <h3 class="mb-3">Pengumuman</h3>
                <ul class="list-group">
                    <li class="list-group-item">Tanggal Pemilihan Ketua Osis</li>
                    <li class="list-group-item bg-light font-weight-bold">18-07-1945</li>
                    <li class="list-group-item">Jumlah Siswa</li>
                    <li class="list-group-item bg-light font-weight-bold">1000</li>
                    <li class="list-group-item">Jumlah Calon Ketua Osis</li>
                    <li class="list-group-item bg-light font-weight-bold">5</li>
                </ul>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="bg-white border rounded p-3">
                <h3 class="mb-3">Berita</h3>
                @forelse($berita as $key=>$value)
                <div class="media border p-2 rounded mb-3">
                    <div class="media-body">
                        <h5 class="mt-0 mb-1">{{$value->judulberita}}</h5>
                        <p align="justify">
                            {{Str::limit($value->deskripsi, 200)}}
                        </p>
                        <a href="/detaillaman/{{$value->id}}">Selengkapnya...</a>
                    </div>
                    <img class="ml-3 rounded" src="./img/{{$value->gambar}}" width="150" height="150" alt="Generic placeholder image">
                </div>
                @empty
                <div class="media border p-2 rounded mb-3">
                    <h2>-- Tidak Ada Berita --</h2>
                </div>
                @endforelse
            </div>
        </div>
    </div>
</div>
@endsection