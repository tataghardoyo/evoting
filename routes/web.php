<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'BerandaController@index');

Auth::routes(['register' => false]);

// Route::get('/home', 'HomeController@index')->name('home');

// Route::middleware('role:admin')->get('/dashboard', function(){
//     return 'Dashboard';
// })->name('dashboard');

Route::get('/detaillaman/{berita_id}','BerandaController@show');


Route::group(['middleware' => 'auth'], function () {
    Route::get('/changepwd', 'PasswordController@index');
});


Route::group(['middleware' => ['role:user']], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/visimisi', 'VisiMisiController@show');
});

Route::group(['middleware' => ['role:admin']], function () {
    Route::get('/dashboard', 'AdminController@index')->name('dashboard');
    Route::get('/createsiswa', 'AdminController@create');
    Route::get('/editsiswa', 'AdminController@edit');

    Route::get('/calonmanajemen', 'CalonManajemenController@index');
    Route::get('/createcalon', 'CalonManajemenController@create');
    Route::get('/editcalon', 'CalonManajemenController@edit');

    Route::get('/votemanajemen', 'VoteManajemenController@index');
    Route::get('/settingvote', 'VoteManajemenController@edit');

    Route::get('/pengumumanmanajemen', 'PengumumanManajemenController@index');
    Route::get('/createpengumuman', 'PengumumanManajemenController@create');
    Route::get('/editpengumuman/{berita_id}', 'PengumumanManajemenController@edit');
    Route::post('/createpengumuman', 'PengumumanManajemenController@store');
    Route::put('/editpengumuman/{berita_id}', 'PengumumanManajemenController@update');
    Route::get('/hapuspengumuman/{berita_id}', 'PengumumanManajemenController@destroy')->name('task.destroy');
});
